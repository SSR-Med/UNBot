import dotenv from 'dotenv';
// Set config
dotenv.config();
export const api_url = process.env.API_URL;