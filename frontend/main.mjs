import { postQuestionProfessor,postInformationProfessor,postTestProfessor } from "./src/services/professor.mjs"
import { createRequire } from "module";

const require = createRequire(import.meta.url);

const { Client } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');
//const axios = require("axios")


//const SERVER_URL = "http://127.0.0.1:8000"

postTestProfessor("test")
      .then((res) => console.log(res.body))

//Client creation

const client = new Client({
    webVersionCache: {
      type: "remote",
      remotePath:
        "https://raw.githubusercontent.com/wppconnect-team/wa-version/main/html/2.2412.54.html",
    },
  });

client.on('ready', () => {
    console.log('Client is ready!');
});

//Client connection with the web user
client.on('qr', qr => {
    qrcode.generate(qr, {small: true});
});

client.initialize();


client.on('message_create', message => {
	if (message.body === '!ping') {
		// reply back "pong" directly to the message
		message.reply('pong');
	}
});

client.on('message_create', message => {

  const words= message.body.split(' ')
  const orderWord= words[0]
  const messageText=words.slice(1).join(" ")

	if ( orderWord === '!opinion') {
  
    postInformationProfessor(messageText)
      .then((res) => message.reply(res.body))
      .then((error) => console.log(error))
	}

  if ( orderWord === '!question') {
    
    postQuestionProfessor(messageText)
      .then((res) => message.reply(res.body))
      .then((error) => console.log(error))
	}

  if(orderWord === '!help'){
    message.reply("Para hacer una pregunta sobre un profesor escribe !question seguido de tu pregunta\n"+
    "Para dar tu opinión sobre un profesor escribe !opinion seguido de tu opinión\n" +
    "Por favor se respetuoso con tus opiniones y preguntas"
    )
  }
  
});



// axios.post(SERVER_URL+"/professor/opinion", {
//     text: "Cómo es el profesor santiago?",
//   })
//   .then((response) => console.log(response.data))
//   .then((error) => console.log(error));


// client.on('message_create', message => {
// 	if (message.body === '!opinion') {
// 		axios.post(SERVER_URL+"/professor/opinion", {
//             text: "Cómo es el profesor santiago?",
//           })
//           .then((response) => message.reply(response.data.body))
//           .then((error) => console.log(error))
        
		
// 	}
// });