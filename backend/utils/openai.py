import sys
import os
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.vectorstores import Chroma
from langchain.chains import RetrievalQA
from langchain_openai import OpenAIEmbeddings
from langchain_community.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
#os.environ["OPENAI_API_KEY"] = ""

prompt_template = """You are a chatbot that helps students find recomendations and opinions about teachers and courses.
Use ONLY the following pieces of context to answer the question at the end.
If you don't know the answer, just say "Lo siento, no sé la respuesta a tu pregunta" don't try to make up an answer.

{context}

Question: {question}
Helpful Answer in Spanish:"""

prompt = PromptTemplate(template=prompt_template, input_variables=["context", "question"])

def get_text(path):
    """Gets the text from a path"""
    print("Obteniendo Información...")
    if path.endswith(".txt"):
        with open(path, "r", encoding='utf-8') as file:
            text = file.read()
        return text

def store_embeddings(text):
    """Stores the embeddings of a text"""
    print("Almacenando embeddings...")
    text_splitter = RecursiveCharacterTextSplitter(        
        chunk_size = 500,
        chunk_overlap  = 250,
        length_function = len,
        is_separator_regex = False
    )

    chunks_text = text_splitter.split_text(text)

    # we create our vectorDB, using the OpenAIEmbeddings tranformer to create
    # embeddings from our text chunks.
    vectordb = Chroma.from_texts(
    chunks_text,
    embedding = OpenAIEmbeddings(),
    persist_directory="./chroma_vector_db",
    collection_name="professors"
    )
    vectordb.persist()
    print("Embeddings almacenados")

    return vectordb

def retrieve_and_answer(vectordb, question):
    """Retrieves and answers a question"""
    llm = ChatOpenAI(
        model_name="gpt-3.5-turbo-16k",
        temperature=0
        )
    chain_type_kwargs = {"prompt": prompt}
    qa_chain = RetrievalQA.from_chain_type(
        llm=llm, 
        retriever=vectordb.as_retriever(search_kwargs={"k": 20}),
        return_source_documents=True,
        chain_type="stuff",
        chain_type_kwargs=chain_type_kwargs
    )

    # we can now execute queries against our Q&A chain
    result = qa_chain({'query': question})
    return result

if __name__ == "__main__":
    # Extract Information
    text = get_text("data.txt")
    # Store Embeddings
    vectordb = store_embeddings(text)
    yellow = "\033[0;33m"
    green = "\033[0;32m"
    # Block Code Q&A
    while True:
        question = input(f"{yellow}Escriba su pregunta: \n")
        if question  == "exit" or question  == "quit" or question  == "q" or question == "f":
            print('Saliendo...')
            sys.exit()
        if question == "":
            continue
        print()
        print(f"{green}Buscando recomendación...")
        result = retrieve_and_answer(vectordb, question)
        print(f"{green}{result['result']}")
        print()
