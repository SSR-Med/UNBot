import axios from "axios";

import { api_url } from "../../config.mjs";

// Create an Axios instance with default options
export default axios.create({
    baseURL: api_url
})