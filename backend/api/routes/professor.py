from typing import Any
import os

from fastapi import APIRouter

from schemas.question import Question
from utils.openai import get_text, store_embeddings,retrieve_and_answer
from core.config import settings

router = APIRouter()

os.environ["OPENAI_API_KEY"] = settings.openai_api_key

text = get_text("../data/model_food.txt")
vectordb = store_embeddings(text)

@router.post("/test")
def test_connection(body: Question):
    """
    Route for TESTING connection

    Args:
        body (Question): Doesn't really matter
    Returns:
        A text saying if it's connected if, indeed, it is connected
    """

    payload= {"body": "It's connected"}
    
    return payload

@router.post("/information")
def posting_opinion(body: Question):
    """
    TODO: Endpoint to add opinion for a professor.

    Args:
        body (Question): Request body containing the text with the professor's information

    Returns:
        dict: Opinion based on data.txt
    """

    opinion = " ".join(body.text.splitlines())

    vectordb.add_texts([opinion])
    # Send request to model for prediction
    payload = {"body": "Opinión guardada de manera exitosa" }

    return payload

@router.post("/question")
def asking_opinion(body: Question):
    """
    Endpoint to ask opinion for a professor.

    Args:
        body (Question): Request body containing the text with the professor's information

    Returns:
        dict: Opinion based on data.txt
    """
    
    question_text = " ".join(body.text.splitlines())

    green = "\033[0;32m" 
        
    print()
    print(f"{green}Buscando recomendación...")
    result = retrieve_and_answer(vectordb, question_text)
    print(f"{green}{result['result']}")
    print()

    payload ={"body": result['result']}
    
    return payload