import pandas as pd
import math

data = pd.read_excel('./data/data.xlsx')
f = open('./data/model_food.txt', 'a', encoding='utf-8')

new_post = False

for values in data.values:
    if new_post:
        if not pd.isna(values[1]):
            f.write(str(values[1]) + " ||| ")
        else:
            new_post = False
            f.write('\n\n')

    if not pd.isna(values[0]):
        if not values[0].startswith("https") and not values[0].isnumeric():  
            new_post = True
            f.write("Pregunta: " + str(values[0]) + '    Respuestas:')
            continue

f.close()