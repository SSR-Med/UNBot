# UNBot

## Description

Recommendation system based on student's opinions of professors with the implementation of the openai api and their rag functionalities

## How does it work

After starting the frontend, the program will generate a qr code through which the admin will have to connect to start the bot functionalities. Such functionalities depend on the node module 'whatsapp-web.js' that will grant them to the phone number the admin used.

Such frontend connects to the backend using axios and sending post request to the REST api with the text questions from students. 

## How to run


### API in your local machine

In the backend folder

First create a virtual enviroment and activate it with:

```bash
python -m venv venv
venv\Scripts\activate
```

Add your openai api key in the .env file. 

Then install the requirements and run the api:

```bash
pip install -r "requirements.txt"
fastapi dev main.py
```

Now you can look at [localhost:8000/docs] and test it on your own machine .


### Running the frontend in your local machine

In the frontend folder.

For staring the program and generating the qr for conecting run the commands:

```bash
npm install
npm start
```

After that, connect through a cellphone with the qr capability of whatsapp. 

Now that user phone number will have the bot functionalities.


