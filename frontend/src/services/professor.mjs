import apiClient from './api-client.mjs'

export const postTestProfessor = async (information) => {
    const payload={
        text: information,
    }
    const res = await apiClient.post(`/professor/test/`, payload).then((res) => {return res})

    return res.data;
};

export const postInformationProfessor = async (information) => {
    const payload={
        text: information,
    }
    const res = await apiClient.post(`/professor/information/`, payload).then((res) => {return res})

    return res.data;
};

export const postQuestionProfessor = async (question) => {
    const payload={
        text: question,
    }
    const res = await apiClient.post(`/professor/question/`, payload).then((res) => {return res})

    return res.data;
};