from fastapi import APIRouter

from api.routes import professor

"""
Creation of the news router
"""
api_router = APIRouter()
api_router.include_router(professor.router, prefix="/professor", tags=["professor"])